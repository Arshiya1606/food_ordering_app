						FOOD ORDERING APP

sql 

1)create table adminlogin(
	a_id number primary key,
	a_name varchar2(26),
	a_username varchar2(20) unique,
	a_password varchar2(10)
	);

SQL> desc adminLogin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 A_ID                                      NOT NULL NUMBER
 A_NAME                                             VARCHAR2(26)
 A_USERNAME                                         VARCHAR2(50)
 A_PASSWORD                                         VARCHAR2(10)


2)create table customer(
	customerid number(10) primary key,
	name varchar2(50),
	phoneNumber varchar2(10) unique,
	emailAddress varchar2(25) unique,
	address varchar2(25)
	);

SQL> desc customer;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 CUSTOMERID                                NOT NULL NUMBER(5)
 NAME                                               VARCHAR2(15)
 PHONENUMBER                                        VARCHAR2(10)
 EMAILADDRESS                                       VARCHAR2(15)
 ADDRESS                                            VARCHAR2(25)


3)create table customerLogin(
	username varchar2(10) unique,
	password varchar2(10)
	);

SQL> desc customerlogin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 USERNAME                                           VARCHAR2(50)
 PASSWORD                                           VARCHAR2(10)


4)create table vendor(
	vendorId number(10) primary key,
	vendorName varchar2(25),
	vendorPhone varchar2(10) unique,
	vendorEmail varchar2(25) unique,
	vendorAddress varchar2(50)
	);

SQL> desc vendor;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 VENDORID                                  NOT NULL NUMBER(10)
 VENDORNAME                                         VARCHAR2(15)
 VENDORPHONE                                        VARCHAR2(10)
 VENDOREMAIL                                        VARCHAR2(15)
 VENDORADDRESS                                      VARCHAR2(25)


5)create table vendorSignin(
	vendorUsername varchar2(50) unique,
	vendorPassword varchar2(25)
	);

SQL> desc vendorsignin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 VENDORUSERNAME                                     VARCHAR2(10)
 VENDORPASSWORD                                     VARCHAR2(10)


6)create table foodOrder(
	foodOrderId number(10) primary key,
	location varchar2(25),	
	foodDetailId number(10) foreign key,
	acceptOrCancel varchar2(10)
	);

SQL> desc foodorder;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 FOODORDERID                               NOT NULL NUMBER(5)
 LOCATION                                           VARCHAR2(25)
 FOODDETAILID                                       NUMBER(10)
 ACCEPTORCANCEL                                     VARCHAR2(10)


7)create table foodDetails(
	foodId number(10) primary key,
	foodName varchar2(25),
	foodType varchar2(25),
	foodRate number(10)
	);

SQL> desc fooddetails;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 FOODID                                    NOT NULL NUMBER(10)
 FOODNAME                                           VARCHAR2(25)
 FOODTYPE                                           VARCHAR2(10)
 FOODRATE                                           NUMBER(10)



